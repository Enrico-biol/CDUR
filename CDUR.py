#!/usr/bin/python3

from __future__ import division
import os
import numpy
import scipy
import pandas
import sys
import argparse
import re

from random import shuffle,random,randint,choice,seed
from collections import Counter
from os import system
from pandas import DataFrame
from pandas import *
from Bio import SeqIO

from subprocess import call
from Bio.SeqRecord import SeqRecord
from CDUR_shuffles import third_simple, gc3, dn23
# utils = importr("utils")
# plyr = importr("plyr")
# seqinr = importr("seqinr")

#Constant values
"""
#nt list
nts = ['A','C','G','T']

#Translation Table
tt = {"TTT":"F|Phe","TTC":"F|Phe","TTA":"L|Leu","TTG":"L|Leu","TCT":"S|Ser","TCC":"S|Ser","TCA":"S|Ser","TCG":"S|Ser", "TAT":"Y|Tyr","TAC":"Y|Tyr","TAA":"*|Stp","TAG":"*|Stp","TGT":"C|Cys","TGC":"C|Cys","TGA":"*|Stp","TGG":"W|Trp", "CTT":"L|Leu","CTC":"L|Leu","CTA":"L|Leu","CTG":"L|Leu","CCT":"P|Pro","CCC":"P|Pro","CCA":"P|Pro","CCG":"P|Pro","CAT":"H|His","CAC":"H|His","CAA":"Q|Gln","CAG":"Q|Gln","CGT":"R|Arg","CGC":"R|Arg","CGA":"R|Arg","CGG":"R|Arg", "ATT":"I|Ile","ATC":"I|Ile","ATA":"I|Ile","ATG":"M|Met","ACT":"T|Thr","ACC":"T|Thr","ACA":"T|Thr","ACG":"T|Thr", "AAT":"N|Asn","AAC":"N|Asn","AAA":"K|Lys","AAG":"K|Lys","AGT":"S|Ser","AGC":"S|Ser","AGA":"R|Arg","AGG":"R|Arg","GTT":"V|Val","GTC":"V|Val","GTA":"V|Val","GTG":"V|Val","GCT":"A|Ala","GCC":"A|Ala","GCA":"A|Ala","GCG":"A|Ala", "GAT":"D|Asp","GAC":"D|Asp","GAA":"E|Glu","GAG":"E|Glu","GGT":"G|Gly","GGC":"G|Gly","GGA":"G|Gly","GGG":"G|Gly"}
#Following functions or their combinations produce randomized or scrambled nucleotide sequence from input sequence.
#Amino-acid sequence of derived sequence is identical to the input sequence, but nucleotide composition (GC-, nucleotide, or dinucleotide content) may differ slightly for randomized sequences.

def gc3(seq):#this function creates sequence with GC-content close to the GC-content of the input sequence, but counts of nucleotides may differ from input sequence.
    gc=at=0
    for num in xrange(2,len(seq),3):#first calculating A+T and G+C of the input sequence in third codon position
        if seq[num]=='A'or seq[num]=='T':
            at+=1
        elif seq[num]=='G'or seq[num]=='C':
            gc+=1
    at=at/(len(seq)/3.)
    gc=gc/(len(seq)/3.)
    
    seq1=[]
    for num in xrange(2,len(seq),3):#list "seq1" will contain the first two nt of codon, third codon position will contain flags for subsequent randomization. Flags ('_Y_','_R_','_H_',or '_N_') correspond to IUPAC single-letter code, Y-Pyrimindine(C or T), R-Purine(A or G), H-Not G(A or C or T), N-any.
        seq1+=seq[num-2:num],
        if (seq[num]=='T'or seq[num]=='C')and(seq[num-2:num]=='TT'or seq[num-2:num]=='TA'or seq[num-2:num]=='TG'or seq[num-2:num]=='CA'or seq[num-2:num]=='AA'or seq[num-2:num]=='AG'or seq[num-2:num]=='GA'):
            seq1+='_Y_',
        elif (seq[num]=='A'or seq[num]=='G')and(seq[num-2:num]=='TT'or seq[num-2:num]=='CA'or seq[num-2:num]=='AA'or seq[num-2:num]=='AG'or seq[num-2:num]=='GA'):
            seq1+='_R_',
        elif seq[num-2:num+1]=='ATT'or seq[num-2:num+1]=='ATC'or seq[num-2:num+1]=='ATA':
            seq1+='_H_',
        elif (seq[num]=='A'or seq[num]=='G'or seq[num]=='T'or seq[num]=='C')and(seq[num-2:num]=='TC'or seq[num-2:num]=='CT'or seq[num-2:num]=='CC'or seq[num-2:num]=='CG'or seq[num-2:num]=='AC'or seq[num-2:num]=='GT'or seq[num-2:num]=='GC'or seq[num-2:num]=='GG'):
            seq1+='_N_',
        else: seq1+=seq[num],
    seq2=''#"seq2" will contain the derived sequence, approproate nucleotide is chosen for flags in "seq1", according to GC-content
    for i in seq1:
        if i == '_Y_':
            x=random()
            if x<=gc:
                seq2+='C'
            elif gc<x<=gc+at:
                seq2+='T'
            else: seq2+=choice('TC')
        elif i == '_R_':
            x=random()
            if x<=gc:
                seq2+='G'
            elif gc<x<=gc+at:
                seq2+='A'
            else: seq2+=choice('AG')
        elif i == '_H_':
            x=random()
            if x<=gc:
                seq2+='C'
            elif gc<x<=gc+at:
                seq2+=choice('AT')
            else: seq2+=choice('ATC')
        elif i == '_N_':
            x=random()
            if x<=gc:
                seq2+=choice('GC')
            elif gc<x<=gc+at:
                seq2+=choice('AT')
            else: seq2+=choice('AGTC')
        else: seq2+=i
    seq=seq2
    return seq   


def third_simple(seq):#this function creates scrambled sequence with the numbers of each nucleotide identical to the input sequence.
    Y=[]
    seq1=[]
    for num in xrange(2,len(seq),3):
        if (seq[num]=='T' or seq[num]=='C')and(seq[num-2:num]=='TT'or seq[num-2:num]=='TC'or seq[num-2:num]=='TA'or seq[num-2:num]=='TG'or seq[num-2:num]=='CT'or seq[num-2:num]=='CC'or seq[num-2:num]=='CA'or seq[num-2:num]=='CG'or seq[num-2:num]=='AT'or seq[num-2:num]=='AC'or seq[num-2:num]=='AA'or seq[num-2:num]=='AG'or seq[num-2:num]=='GU'or seq[num-2:num]=='GC'or seq[num-2:num]=='GA'or seq[num-2:num]=='GG'):
            Y+=seq[num],
            seq1+=seq[num-2:num],'_Y_',
        else:seq1+=seq[num-2:num+1],
    #now "seq1" contains flag '_Y_' in the third position of all codons, where C->T or T->C shuffling preserves the aminoacid sequence (i.e. PHE, SER etc.).
    #C and T from the original sequence in this case would be extracted into list "Y"
    shuffle(Y)#shuffling of list "Y". For example, before shuffling "Y" is ['C','T','C']; after - ['T','C','C']or['C','C','T']or['C','T','C']
    seq2=''
    for i in xrange(len(seq1)):
        if seq1[i]=='_Y_':seq2+=Y.pop(0)#now elements of "Y" are inserted back into the sequence instead of '_Y_', but in a different order compared to the input sequence
        else:seq2+=seq1[i]
    seq=seq2

    R=[]#similar to the previous step, but A and G are shuffled
    seq1=[]
    for num in xrange(2,len(seq),3):
        if (seq[num]=='A' or seq[num]=='G')and(seq[num-2:num]=='TT'or seq[num-2:num]=='TC'or seq[num-2:num]=='CT'or seq[num-2:num]=='CC'or seq[num-2:num]=='CA'or seq[num-2:num]=='CG'or seq[num-2:num]=='AC'or seq[num-2:num]=='AA'or seq[num-2:num]=='AG'or seq[num-2:num]=='GT'or seq[num-2:num]=='GC'or seq[num-2:num]=='GA'or seq[num-2:num]=='GG'):
            R+=seq[num],
            seq1+=seq[num-2:num],'_R_',
        else:seq1+=seq[num-2:num+1],
    shuffle(R)
    seq2=''
    for i in xrange(len(seq1)):
        if seq1[i]=='_R_':seq2+=R.pop(0)
        else:seq2+=seq1[i]
    seq=seq2
    

    H=[]#similar to the previous step, but A,C, and T are shuffled. Affected aminoacids are ILE (three codons), four-codon and four-codon portion of six-codon aminoacids.
    seq1=[]
    for num in xrange(2,len(seq),3):
        if (seq[num]=='A'or seq[num]=='C'or seq[num]=='T')and(seq[num-2:num]=='TC'or seq[num-2:num]=='CT'or seq[num-2:num]=='CC'or seq[num-2:num]=='CG'or seq[num-2:num]=='AT'or seq[num-2:num]=='AC'or seq[num-2:num]=='GT'or seq[num-2:num]=='GC'or seq[num-2:num]=='GG'):
            H+=seq[num],
            seq1+=seq[num-2:num],'_H_',
        else:seq1+=seq[num-2:num+1],
    shuffle(H)
    seq2=''
    for i in xrange(len(seq1)):
        if seq1[i]=='_H_':seq2+=H.pop(0)
        else:seq2+=seq1[i]
    seq=seq2

    N=[]#Shuffling of all four nucleotides, where possible. Affected aminoacids are four-codons and four-codon portion of six-codon aminoacids.
    seq1=[]
    for num in xrange(2,len(seq),3):
        if (seq[num]=='A'or seq[num]=='C'or seq[num]=='T'or seq[num]=='G')and(seq[num-2:num]=='TC'or seq[num-2:num]=='CT'or seq[num-2:num]=='CC'or seq[num-2:num]=='CG'or seq[num-2:num]=='AC'or seq[num-2:num]=='GT'or seq[num-2:num]=='GC'or seq[num-2:num]=='GG'):
            N+=seq[num],
            seq1+=seq[num-2:num],'_N_',
        else:seq1+=seq[num-2:num+1],
    shuffle(N)
    seq2=''
    for i in xrange(len(seq1)):
        if seq1[i]=='_N_':seq2+=N.pop(0)
        else:seq2+=seq1[i]
    seq=seq2        
    return seq
    
    
def dn23(seq):#this function creates a randomized sequence, with dinucleotide frequences in codon position 2-3 close to those of the input sequence (not exact since this only counts dinucleotide frequency in second and third positions).
    aa=ag=ac=at=ga=gg=gc=gt=ca=cg=cc=ct=ta=tg=tc=tt=0
    for num in xrange(2,len(seq),3):#first calculating dinucleotide frequences in codon position 2-3 
        if seq[num-1]=='A':
            if seq[num]=='A':
                aa+=1
            elif seq[num]=='G':
                ag+=1
            elif seq[num]=='C':
                ac+=1
            elif seq[num]=='T':
                at+=1
        elif seq[num-1]=='G':
            if seq[num]=='A':
                ga+=1
            elif seq[num]=='G':
                gg+=1
            elif seq[num]=='C':
                gc+=1
            elif seq[num]=='T':
                gt+=1
        elif seq[num-1]=='C':
            if seq[num]=='A':
                ca+=1
            elif seq[num]=='G':
                cg+=1
            elif seq[num]=='C':
                cc+=1
            elif seq[num]=='T':
                ct+=1
        elif seq[num-1]=='T':
            if seq[num]=='A':
                ta+=1
            elif seq[num]=='G':
                tg+=1
            elif seq[num]=='C':
                tc+=1
            elif seq[num]=='T':
                tt+=1
    aa,ag,ac,at,ga,gg,gc,gt,ca,cg,cc,ct,ta,tg,tc,tt=aa/(len(seq)/3.),ag/(len(seq)/3.),ac/(len(seq)/3.),at/(len(seq)/3.),ga/(len(seq)/3.),gg/(len(seq)/3.),gc/(len(seq)/3.),gt/(len(seq)/3.),ca/(len(seq)/3.),cg/(len(seq)/3.),cc/(len(seq)/3.),ct/(len(seq)/3.),ta/(len(seq)/3.),tg/(len(seq)/3.),tc/(len(seq)/3.),tt/(len(seq)/3.)
    seq2=''
    for num in xrange(2,len(seq),3):#now each codon is replaced with a synonymous codon according to the dinucleotide frequences in codon position 2-3 
        seq2+=seq[num-2:num]
        #print seq2
        if seq[num-1]=='A'and seq[num-2:num+1]!='TAA'and seq[num-2:num+1]!='TAG':
            if seq[num]=='T'or seq[num]=='C':
                space=at+ac
                AT,AC=at/space,ac/space
                x=random()
                if x<=AT:
                    seq2+='T'
                elif AT<x<=AT+AC:
                    seq2+='C'
            elif seq[num]=='A'or seq[num]=='G':
                space=aa+ag
                AA,AG=aa/space,ag/space
                x=random()
                if x<=AA:
                    seq2+='A'
                elif AA<x<=AA+AG:
                    seq2+='G'
            else:seq2+=seq[num]
        elif seq[num-1]=='G'and seq[num-2:num+1]!='TGA'and seq[num-2:num+1]!='TGG':
            if (seq[num-2]=='T'or seq[num-2]=='A')and(seq[num]=='C'or seq[num]=='T'):
                space = gt+gc
                GT,GC=gt/space,gc/space
                x=random()
                if x<=GT:
                    seq2+='T'
                elif GT<x<=GT+GC:
                    seq2+='C'
            elif seq[num-2:num+1]=='AGA'or seq[num-2:num+1]=='AGG':
                space=ga+gg
                GA,GG=ga/space,gg/space
                x=random()
                if x<=GA:
                    seq2+='A'
                elif GA<x<=GA+GG:
                    seq2+='G'
            elif seq[num-2]=='C'or seq[num-2]=='G':
                space=ga+gg+gc+gt
                GA,GG,GC,GT=ga/space,gg/space,gc/space,gt/space
                x=random()
                if x<=GA:seq2+='A'
                elif GA<x<=GA+GG:seq2+='G'
                elif GA+GG<x<=GA+GG+GC:seq2+='C'
                elif GA+GG+GC<x<=GA+GG+GC+GT:seq2+='T'
            else:seq2+=seq[num]
        elif seq[num-1]=='C':
            space=ca+cg+cc+ct
            CA,CG,CC,CT=ca/space,cg/space,cc/space,ct/space
            x=random()
            if x<=CA:seq2+='A'
            elif CA<x<=CA+CG:seq2+='G'
            elif CA+CG<x<=CA+CG+CC:seq2+='C'
            elif CA+CG+CC<x<=CA+CG+CC+CT:seq2+='T'
        elif seq[num-1]=='T':
            if seq[num-2:num+1]=='TTT'or seq[num-2:num+1]=='TTC':
                space = tt+tc
                TT,TC=tt/space,tc/space
                x=random()
                if x<=TT:
                    seq2+='T'
                elif TT<x<=TT+TC:
                    seq2+='C'
            elif seq[num-2:num+1]=='TTA'or seq[num-2:num+1]=='TTG':
                space = ta+tg
                TA,TG=ta/space,tg/space
                x=random()
                if x<=TA:
                    seq2+='A'
                elif TA<x<=TA+TG:
                    seq2+='G'
            elif seq[num-2:num+1]=='ATT'or seq[num-2:num+1]=='ATC'or seq[num-2:num+1]=='ATA':
                space=tt+tc+ta
                TT,TC,TA=tt/space,tc/space,ta/space
                x=random()
                if x<=TA:seq2+='A'
                elif TA<x<=TA+TC:seq2+='C'
                elif TA+TC<x<=TA+TC+TT:seq2+='T'
            elif seq[num-2]=='C'or seq[num-2]=='G':
                space=ta+tg+tc+tt
                TA,TG,TC,TT=ta/space,tg/space,tc/space,tt/space
                x=random()
                if x<=TA:seq2+='A'
                elif TA<x<=TA+TG:seq2+='G'
                elif TA+TG<x<=TA+TG+TC:seq2+='C'
                elif TA+TG+TC<x<=TA+TG+TC+TT:seq2+='T'
            else:seq2+=seq[num]
        else:seq2+=seq[num]
    seq=seq2
    return seq    
    

    
    # main script body that calls the above subroutines

#Shuffle Script
# -*- coding: cp1251 -*-


"""

#Input Data
parser = argparse.ArgumentParser(description='CodonShuffle.')
parser.add_argument('-i', nargs='?', help='Input Filename', required=True, dest="input_file_name")
parser.add_argument('-s', choices=['n3', 'gc3', 'dn23'], nargs='?', help='Type of shuffle', default="n3", dest="random_type")
parser.add_argument('-r', nargs='?', help='Number of replications (int)', default='1000', dest="reps", type=int)
parser.add_argument('-m', nargs='?', help='Pass in motif file (with path if necessary)', dest="motifs")
parser.add_argument('-o', nargs='?', help='Path to folder to output files. For convenience, please use the full PATH. Add trailing /', dest='out_folder', default = "./")
parser.add_argument('-d', nargs='?', help="Delete files containing processed seq and shuffled seq. Type 1 to delete, leave empty to keep.", dest="delete", default = False)
parser.add_argument('--seed', type=int, nargs='?', dest='randomseed', help='Optional integer for random seed', const=99)
args = parser.parse_args()

if args.randomseed is not None:
    seed(args.randomseed)

types_of_rnd=args.random_type

infile=open(args.input_file_name,'r')
names_list=[]
data={}
for line in infile:
    if line[0]=='>':
        strain=line
        data[strain]=''
        names_list+=strain,
    else:
        for liter in line:
            if liter!='\n' and liter!='-' and liter!='~':
                data[strain]+=liter
infile.close()


out_names=[]
for strain in names_list:
    seq_name=''
    for liter in strain:
        if liter =='\\' or liter =='/' or liter ==' ' or liter =='-' or liter ==',' or liter=='|' or liter==':': # compatible file names
            seq_name+='_'
        else:
            seq_name+=liter
    inseq_file=open(args.out_folder + seq_name[1:-1]+'.fas','w')
    inseq_file.write(strain+data[strain]+'\n')
    inseq_file.close()
#     bat_enc='chips  -seqall '+seq_name[1:-1]+'.fas -nosum -outfile '+seq_name[1:-1]+'.enc -auto\n'
#     system(bat_enc)

    outfile=open(args.out_folder + seq_name[1:-1]+'_'+args.random_type+'.fasta','w') #Create the file with wild type in the first position
    outfile.write(strain+data[strain]+'\n')
    outfile.close()


    outfile=open(args.out_folder + seq_name[1:-1]+'_'+args.random_type+'.fasta','a') #Append permuted sequence
    for i in range(args.reps):
        outseq=data[strain]
        if args.random_type =='gc3':
            outseq=gc3(outseq)
        elif args.random_type=='n3':
            outseq=third_simple(outseq)
        elif args.random_type == 'dn23':
            outseq = dn23(outseq)
        outfile.write('>replicate_'+str(i+1)+'\n'+outseq+'\n')
    outfile.close()
#     bat_enc='chips  -seqall '+seq_name[1:-1]+'_'+args.random_type+'.fas -nosum -outfile '+seq_name[1:-1]+'_'+args.random_type+'.enc -auto\n'
#     system(bat_enc)

    if args.motifs is not None:
        os.system(os.path.dirname(os.path.realpath('__file__'))+"/shmsim "+args.out_folder + seq_name[1:-1]+'_'+args.random_type+'.fasta ' + args.motifs + ' > ' + args.out_folder + seq_name[1:-1] + '_' + args.random_type + 'results.txt')

        if args.delete:
            try:
                os.remove(args.out_folder + seq_name[1:-1]+'.fas')
            except Exception:
                pass

            try:
                os.remove(args.out_folder+seq_name[1:-1]+"_"+args.random_type+'.fasta')
            except Exception:
                pass
    else:
	    os.system(os.path.dirname(os.path.realpath('__file__'))+"/shmsim "+seq_name[1:-1]+'_'+args.random_type+'.fasta > ' + seq_name[1:-1] + '_' + args.random_type + 'results.txt')
