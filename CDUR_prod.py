#!/usr/bin/python2.7

from __future__ import division
import os
import numpy
import scipy
import pandas
#import statsmodels
import sys
import argparse
import re
import Shuffles

from random import shuffle,random,randint,choice,seed
from collections import Counter
from os import system
from pandas import DataFrame
from pandas import *
from Bio import SeqIO
from subprocess import call
from Bio.SeqRecord import SeqRecord


#Input Data
parser = argparse.ArgumentParser(description='CodonShuffle.')
parser.add_argument('-i', nargs='?', help='Input Filename', required=True, dest="input_file_name")
parser.add_argument('-s', choices=['n3', 'gc3', 'dn23'], nargs='?', help='Type of shuffle', default="n3", dest="random_type")
parser.add_argument('-r', nargs='?', help='Number of replications (int)', default='1000', dest="reps", type=int)
parser.add_argument('-m', nargs='?', help='Pass in motif file', dest="motifs")
parser.add_argument('-o', nargs='?', help='path to folder to output files. For convenience, please use the full PATH. Add trailing /', dest='out_folder', default = "./")
parser.add_argument('-d', nargs='?', help="Delete files containing processed seq and shuffled seq. Type 1 to delete, leave empty to keep.", dest="delete", default = False)
parser.add_argument('--seed', type=int, nargs='?', dest='randomseed', help='Optional integer for random seed', const=99)
args = parser.parse_args()

if args.randomseed is not None:
    seed(args.randomseed)

types_of_rnd=args.random_type

infile=open(args.input_file_name,'r')
names_list=[]
data={}
for line in infile:
    if line[0]=='>':
        strain=line
        data[strain]=''
        names_list+=strain,
    else:
        for liter in line:
            if liter!='\n' and liter!='-' and liter!='~':
                data[strain]+=liter
infile.close()
    

out_names=[]
for strain in names_list:
    seq_name=''
    for liter in strain:
        if liter =='\\' or liter =='/' or liter ==' ' or liter =='-' or liter ==',' or liter=='|' or liter==':': # compatible file names
            seq_name+='_'
        else:
            seq_name+=liter
    inseq_file=open(args.out_folder + seq_name[1:-1]+'.fas','w')
    inseq_file.write(strain+data[strain]+'\n')
    inseq_file.close()
#     bat_enc='chips  -seqall '+seq_name[1:-1]+'.fas -nosum -outfile '+seq_name[1:-1]+'.enc -auto\n'
#     system(bat_enc)

    outfile=open(args.out_folder + seq_name[1:-1]+'_'+args.random_type+'.fasta','w') #Create the file with wild type in the first position
    outfile.write(strain+data[strain]+'\n')
    outfile.close()


    outfile=open(args.out_folder + seq_name[1:-1]+'_'+args.random_type+'.fasta','a') #Append permuted sequence
    for i in range(args.reps):
        outseq=Shuffle(data[strain])
        if args.random_type =='gc3':
            outseq=Shuffle.gc3()
        elif args.random_type=='n3':
            outseq=Shuffle.third_simple()
        elif args.random_type == 'dn23':
            outseq = Shuffle.dn23()
        outfile.write('>replicate_'+str(i+1)+'\n'+outseq+'\n')
    outfile.close()
#     bat_enc='chips  -seqall '+seq_name[1:-1]+'_'+args.random_type+'.fas -nosum -outfile '+seq_name[1:-1]+'_'+args.random_type+'.enc -auto\n'
#     system(bat_enc)

    if args.motifs is not None:
	    os.system("~/CDUR/shmsim "+seq_name[1:-1]+'_'+args.random_type+'.fasta ' + args.motifs + ' > ' + seq_name[1:-1] + '_' + args.random_type + 'results.txt')

        if args.delete:
            try:
                os.remove(args.out_folder + seq_name[1:-1]+'.fas')
            except Exception:
                pass

            try:
                os.remove(f"{args.out_folder}{seq_name[1:-1]}_{args.random_type}.fasta")
            except Exception:
                pass
    else:
	    os.system("~/CDUR/shmsim "+seq_name[1:-1]+'_'+args.random_type+'.fasta > ' + seq_name[1:-1] + '_' + args.random_type + 'results.txt')
