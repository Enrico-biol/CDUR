#ifndef GLOBAL_H
#define GLOBAL_H

// TODO: get rid of this file!

/*
  Global definitions
*/

#include "typedefs.h" // moved global typedefs here

//TODO: These constants should be in a parameters file.

#define EXHAUSTIVE_SEARCH_THRESHOLD 4

#define MAX_STUCK_REPEAT_COUNT 100

#define MAX_ATTEMPTS 50

#define MAX_ISLAND_SIZE 10000

#define MAX_DB_SIZE 100000000

#define MAX_WRITTEN_RESULTS 95

#define MAX_SOLUTION_ATTEMPTS 1

typedef int score_type;


#endif
